package com.example.android.demoapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.*;

import com.example.android.demoapp.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.R.attr.name;

public class CadastrarActivity extends AppCompatActivity {

    private EditText ed_password;
    private EditText ed_email;
    private EditText ed_password2;
    private ProgressDialog prgDialog;
    private EditText ed_nome;
    private EditText ed_sobre_nome;
    private TextView tvErrorMessageEmail;
    private TextView tvErrorMessagePassword;
    private EditText ed_telefone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.cadastrar));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ed_nome = (EditText) findViewById(R.id.ed_nome);
        ed_sobre_nome = (EditText) findViewById(R.id.ed_sobre_nome);
        ed_email = (EditText) findViewById(R.id.ed_email);
        ed_telefone = (EditText) findViewById(R.id.ed_telefone);
        ed_password = (EditText) findViewById(R.id.ed_password);
        ed_password2 = (EditText) findViewById(R.id.ed_password_2);
        tvErrorMessageEmail = (TextView) findViewById(R.id.tv_error_message_email_2);
        tvErrorMessagePassword = (TextView) findViewById(R.id.tv_error_message_password_2);

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Aguarde...");
        prgDialog.setCancelable(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Method gets triggered when Register button is clicked
     *
     * @param view
     */
    public void registerUser(View view) {
        // Get Nome ET value
        String nome = ed_nome.getText().toString();
        // Get Nome ET value
        String sobreNome = ed_sobre_nome.getText().toString();
        // Get Email ET control value
        String email = ed_email.getText().toString();
        // Get Telephone ET control value
        String telefone = ed_telefone.getText().toString();
        // Get Password ET control value
        String password = ed_password.getText().toString();
        String password2 = ed_password2.getText().toString();

//        // Get telephone number from system service
//        TelephonyManager tMgr = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//        String telefone = tMgr.getLine1Number();

        //Get date string with current time
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String data = dateFormat.format(date);
//        String data = "2";

        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();
        // When Name Edit View, Email Edit View and Password Edit View have values other than Null
        if(Utility.isNotNull(nome) && Utility.isNotNull(sobreNome) && Utility.isNotNull(email) && Utility.isNotNull(password)
                && Utility.isNotNull(password2)){
            if(!Utility.validate(email)) {
                tvErrorMessageEmail.setVisibility(View.VISIBLE);
            } else if (!password.equals(password2)) {
                tvErrorMessagePassword.setVisibility(View.VISIBLE);
            } else {
                tvErrorMessageEmail.setVisibility(View.INVISIBLE);
                tvErrorMessagePassword.setVisibility(View.INVISIBLE);
                // Put Http parameter name with value of Name Edit View control
                params.put("email", email);
                params.put("password", password);
                params.put("nome", nome);
                params.put("sobrenome", sobreNome);
                params.put("datainicio", data);
                params.put("telefone", telefone);
                // Invoke RESTful Web Service with Http parameters
                invokeWS(params);
            }
        } else{
            Toast.makeText(getApplicationContext(), "Por favor, preencher formulario. ", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void invokeWS(RequestParams params) {
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://10.0.2.2:8084/RaziosWebService/register/doregister", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    Log.d("onSuccess", new String(responseBody));
                    JSONObject obj = new JSONObject(new String(responseBody));
                    // JSON Object
//                    JSONObject obj = new JSONObject("200 OK");
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
//                        setDefaultValues();
                        // Display successfully registered message using Toast
                        Toast.makeText(getApplicationContext(), "You are successfully registered!", Toast.LENGTH_LONG).show();
                        navigatetoLoginActivity();
                    }
                    // Else display error message
                    else {
//                        errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] bytes, Throwable throwable) {
                    // Hide Progress Dialog
                    prgDialog.hide();
                    // When Http response code is '404'
                    if(statusCode == 404){
                        Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '500'
                    else if(statusCode == 500){
                        Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code other than 404, 500
                    else{
                        Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }
            }
        });
    }

    /**
     * Method which navigates from Register Activity to Login Activity
     */
    public void navigatetoLoginActivity(){
        Intent loginIntent = new Intent(getApplicationContext(),LoginActivity2.class);
        // Clears History of Activity
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }
//
//    /**
//     * Set degault values for Edit View controls
//     */
//    public void setDefaultValues(){
//        ed_email.setText("");
//        ed_nome.setText("");
//        ed_sobre_nome.setText("");
//        ed_password.setText("");
//        ed_password2.setText("");
//    }

}
