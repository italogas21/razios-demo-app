package com.example.android.demoapp.database;

import android.provider.BaseColumns;

/**
 * Created by italo on 28/09/17.
 */
public class PedidosContract {

    /* OrdersEntry is an inner class that defines the contents of the task table */
    public static final class PedidosEntry implements BaseColumns {

        // Task table and column names
        public static final String NOME_TABELA = "pedidos";

        // Since TaskEntry implements the interface "BaseColumns", it has an automatically produced
        // "_ID" column in addition to the two below
        public static final String COLUNA_CATEGORIA = "categoria";
        public static final String COLUNA_SUBCATEGORIA = "sub_categoria";
        public static final String COLUNA_DATA = "data";
        public static final String COLUNA_NUM_ANUNCIOS = "num_de_anuncios";


    }
}
