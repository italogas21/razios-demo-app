package com.example.android.demoapp;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by italo on 01/08/17.
 */

public class CategoriasAdapter extends RecyclerView.Adapter<CategoriasAdapter.ListItemViewHolder> {

    private static final String TAG = CategoriasAdapter.class.getSimpleName();

    private static int viewHolderCount;

    private int numberOfItems;
    private String[] categoriasArray;
    private String[] descricoesArray;
    private Drawable[] imagesArray;

    /*
     * An on-click handler that we've defined to make it easy for an Activity to interface with
     * our RecyclerView
     */
    final private CategoriasAdapterOnClickHandler mClickHandler;

    /**
     * The interface that receives onClick messages.
     */
    public interface CategoriasAdapterOnClickHandler {
        void onClick(String item);
    }

    public CategoriasAdapter(int numberOfItems, CategoriasAdapterOnClickHandler clickHandler, Context context) {
        this.numberOfItems = numberOfItems;
        this.mClickHandler = clickHandler;
        viewHolderCount = 0;

        categoriasArray = context.getResources().getStringArray(R.array.categorias);
        descricoesArray = context.getResources().getStringArray(R.array.descricoes);

        imagesArray = new Drawable[]{
                context.getResources().getDrawable(R.drawable.ic_laptop_windows_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_build_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_cake_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_kitchen_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_local_library_black_24dp),
                context.getResources().getDrawable(R.drawable.ic_face_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_laptop_windows_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_web_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_local_hospital_black_48dp),
                context.getResources().getDrawable(R.drawable.ic_directions_car_black_48dp) };
    }

    /**
     * Called when RecyclerView needs a new {@link ListItemViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ListItemViewHolder, int)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ListItemViewHolder, int)
     */
    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.list_categorias;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        ListItemViewHolder viewHolder = new ListItemViewHolder(view);

//        viewHolder.viewHolderIndex.setText("ViewHolder index: " + viewHolderCount);

//        int backgroundColorForViewHolder = ColorUtils.getViewHolderBackgroundColorFromInstance(context, viewHolderCount);
//        viewHolder.itemView.setBackgroundColor(backgroundColorForViewHolder);

        viewHolderCount++;
        Log.d(TAG, "onCreateViewHolder: number of ViewHolders created: " + viewHolderCount);
        return viewHolder;

    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ListItemViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ListItemViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(ListItemViewHolder, int)} instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
//        Message viewHolderData = mApiData.get(position);
        holder.titleTextView.setText(categoriasArray[position]);
        holder.descriptionTextView.setText(descricoesArray[position]);
        holder.imageView.setImageDrawable(imagesArray[position]);
//        holder.imageTView.setImageURI(viewHolderData.);
//        holder.bind(position);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return categoriasArray.length;
    }

    /**
     * This method is used to set the data on a CategoriasAdapter if we've already
     * created one. This is handy when we get new data from the web but don't want to create a
     * new ForecastAdapter to display it.
     *
     * @param categoriasData The new data to be displayed.
     */
    public void setAdapterData(String[] categoriasData) {
        this.categoriasArray = categoriasData;
        notifyDataSetChanged();
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView titleTextView;
        private TextView descriptionTextView;
        private ImageView imageView;

        public ListItemViewHolder(View itemView) {
            super(itemView);

//            Typeface tf_tilte = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/SignikaNegative-SemiBold.ttf");
//            Typeface tf_description = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/SignikaNegative-Regular.ttf");

            titleTextView = (TextView) itemView.findViewById(R.id.tv_item_text);
//            titleTextView.setTypeface(tf_tilte);
            descriptionTextView = (TextView) itemView.findViewById(R.id.firstLine);
//            descriptionTextView.setTypeface(tf_description);
            imageView = (ImageView) itemView.findViewById(R.id.icon);

            itemView.setOnClickListener(this);

        }

        /**
         * This gets called by the child views during a click.
         *
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            Log.d(CategoriasAdapter.class.getSimpleName(), "adapter position: " + String.valueOf(adapterPosition));
            mClickHandler.onClick(String.valueOf(adapterPosition+1));
        }

//        /**
//         * A method we wrote for convenience. This method will take an integer as input and
//         * use that integer to display the appropriate text within a list item.
//         * @param listIndex Position of the item in the list
//         */
//        void bind(int listIndex) {
//
//            CharSequence titleTextViewText = titleTextView.getText();
//            titleTextView.setText( String.valueOf(listIndex) + ": " + titleTextViewText );
//
//        }

    }

}
