package com.example.android.demoapp.database;

import java.io.Serializable;

/**
 * Created by italo on 13/10/17.
 */
public class AnuncioInfo implements Serializable {
    private String servicos;
    private String descricao;
    private String categoria;
    private String subcategoria;
    private String email;
    private String nome;
    private String sobrenome;
    private String datainicio;
    private String telefone;

    public AnuncioInfo(String servicos, String descricao, String categoria, String subcategoria, String email, String nome, String sobrenome, String datainicio, String telefone) {
        this.servicos = servicos;
        this.descricao = descricao;
        this.categoria = categoria;
        this.subcategoria = subcategoria;
        this.email = email;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.datainicio = datainicio;
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getDatainicio() {
        return datainicio;
    }

    public void setDatainicio(String datainicio) {
        this.datainicio = datainicio;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getServicos() {
        return servicos;
    }

    public void setServicos(String servicos) {
        this.servicos = servicos;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }
}
