package com.example.android.demoapp;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class CategoriasActivity extends AppCompatActivity implements CategoriasAdapter.CategoriasAdapterOnClickHandler {

    private static final int NUM_LIST_ITEMS = 20;
    private RecyclerView mRecyclerView;
    private CategoriasAdapter mAdapter;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.selecionar_categoria));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_categorias);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setHasFixedSize(true);

        /*
         * CategoriasAdapter responsavel por mostrar cada item na lista.
         */
        mAdapter = new CategoriasAdapter(NUM_LIST_ITEMS, this, getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(String item) {
        Toast.makeText(this, item, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(CategoriasActivity.this, EspecializacoesActivity.class);
        intent.putExtra("categoria", item);
        startActivity(intent);
    }
}
