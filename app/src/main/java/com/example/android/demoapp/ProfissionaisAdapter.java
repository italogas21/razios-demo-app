package com.example.android.demoapp;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.demoapp.database.AnuncioInfo;
import com.example.android.demoapp.database.Pedido;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by italo on 29/08/17.
 */

public class ProfissionaisAdapter extends RecyclerView.Adapter<ProfissionaisAdapter.ListItemViewHolder>{

    private static int viewHolderCount;
    private final Context ctx;
    private int numberOfItems;
    private List<AnuncioInfo> profissionais;

    final private ProfissionaisAdapterOnClickHandler mClickHandler;

    public void setProfissionais(List<AnuncioInfo> profissionais) {
        this.profissionais = profissionais;
    }

    public interface ProfissionaisAdapterOnClickHandler {
        void onClick(AnuncioInfo item);
    }

    public ProfissionaisAdapter(int numListItems, ProfissionaisAdapterOnClickHandler clickHandler, Context context) {
        this.numberOfItems = numListItems;
        mClickHandler = clickHandler;
        this.ctx = context;
    }

    @Override
    public ProfissionaisAdapter.ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.list_pedidos;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        ProfissionaisAdapter.ListItemViewHolder viewHolder = new ProfissionaisAdapter.ListItemViewHolder(view);

        viewHolderCount++;
        Log.d(this.getClass().getSimpleName(), "onCreateViewHolder: number of ViewHolders created: " + viewHolderCount);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(ProfissionaisAdapter.ListItemViewHolder holder, int position) {
        Log.d(this.getClass().getSimpleName(), "#" + position);
        AnuncioInfo pedido = profissionais.get(position);

        String[] categorias = this.ctx.getResources().getStringArray(R.array.categorias);

        holder.nomeTextView.setText(pedido.getNome());
        holder.descricaoTextView.setText(pedido.getDescricao());
    }

    @Override
    public int getItemCount() {
        return profissionais.size();
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nomeTextView;
        private TextView descricaoTextView;

        public ListItemViewHolder(View itemView) {
            super(itemView);

            nomeTextView = (TextView) itemView.findViewById(R.id.tv_nome);
            descricaoTextView = (TextView) itemView.findViewById(R.id.tv_descricao);

            itemView.setOnClickListener(this);

        }

        /**
         * This gets called by the child views during a click.
         *
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            Log.d(CategoriasAdapter.class.getSimpleName(), "adapter position: " + String.valueOf(adapterPosition));
            AnuncioInfo itemClicked = profissionais.get(adapterPosition);
            mClickHandler.onClick(itemClicked);
        }
    }

}