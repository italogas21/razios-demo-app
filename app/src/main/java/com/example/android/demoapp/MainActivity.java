package com.example.android.demoapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.demoapp.database.AnuncioInfo;
import com.example.android.demoapp.database.ContextHelper;
import com.example.android.demoapp.database.Pedido;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PedidosAdapter.PedidosAdapterOnClickHandler {

    private static final int NUM_LIST_ITEMS = 10;

    private NavigationView navigationView;
    private RecyclerView mRecyclerView;
    private PedidosAdapter mAdapter;

    private ProgressDialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Store a new instance of a Context for later use
         */
        ContextHelper.getInstance().setContext(this);

        //verifica se usuario esta logado
        if (!(SaveSharedPreference.getUserName(MainActivity.this).length() == 0)) {
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View header = navigationView.getHeaderView(0);
            TextView tv_uname = (TextView) header.findViewById(R.id.tv_username);
            tv_uname.setText(SaveSharedPreference.getUserName(MainActivity.this));
            TextView tv_email = (TextView) header.findViewById(R.id.tv_email);
            tv_email.setText(SaveSharedPreference.getUserEmail(MainActivity.this));

        }

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Buscando informações dos profissionais ... ");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbar.setTitle(getResources().getString(R.string.meus_pedidos));
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CategoriasActivity.class);
                startActivity(intent);

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        /*
         *   Starts a new Loader with ID 0.
         */
        //getLoaderManager().initLoader( 0, null, this );

        /*
         * Using findViewById, we get a reference to our RecyclerView from xml. This allows us to
         * do things like set the adapter of the RecyclerView and toggle the visibility.
         */
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_data_info);

        /*
         * A LinearLayoutManager is responsible for measuring and positioning item views within a
         * RecyclerView into a linear list. This means that it can produce either a horizontal or
         * vertical list depending on which parameter you pass in to the LinearLayoutManager
         * constructor. By default, if you don't specify an orientation, you get a vertical list.
         * In our case, we want a vertical list, so we don't need to pass in an orientation flag to
         * the LinearLayoutManager constructor.
         *
         * There are other LayoutManagers available to display your data in uniform grids,
         * staggered grids, and more! See the developer documentation for more details.
         */
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        /*
         * Use this setting to improve performance if you know that changes in content do not
         * change the child layout size in the RecyclerView
         */
        mRecyclerView.setHasFixedSize(true);

        /*
         * The GreenAdapter is responsible for displaying each item in the list.
         */
        mAdapter = new PedidosAdapter(this, NUM_LIST_ITEMS, this);
        mRecyclerView.setAdapter(mAdapter);
//
//        /*
//         * The ProgressBar that will indicate to the user that we are loading data. It will be
//         * hidden when no data is loading.
//         */
//        mLoadingIndicator = (ProgressBar) findViewById(R.id.pb_loading_indicator);

    }

    @Override
    protected void onRestart() {
        mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());
        super.onRestart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate( R.menu.main, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();

        if (itemThatWasClickedId == R.id.action_search) {
            //TODO
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_fale_conosco) {

        } else if (id == R.id.nav_encontre) {
            Intent intent = new Intent(this, CategoriasActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pedidos) {

        } else if (id == R.id.nav_cadastro) {
            Intent intent = new Intent(this, CadastrarProfissionalActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_minha_conta) {
            if ((SaveSharedPreference.getUserName(MainActivity.this).length() == 0)) {
                Intent intent = new Intent(this, LoginActivity2.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, MinhaContaActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.nav_config) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openLoginActivity(View view) {
        //usuario nao esta logado
        if ((SaveSharedPreference.getUserName(MainActivity.this).length() == 0)) {
            Intent intent = new Intent(this, LoginActivity2.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, MinhaContaActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(Pedido pedido) {

        RequestParams params = new RequestParams();
        params.put("categoria", pedido.getCategoria());
        params.put("subcategoria", pedido.getCategoria());

        invokeWS(params);

//        Intent intent = new Intent(this, ProfissionaisActivity.class);
//        Iterator<Pedido> iterator = pedidos.iterator();
//        int i = 1;
//        while (iterator.hasNext()) {
//            Pedido pedido = iterator.next();
//            intent.putExtra("pedido" + i, (Serializable) pedido);
//            i++;
//        }
//        startActivity(intent);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                selectItem(position);
            Toast.makeText(view.getContext(), "position: " + position, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method that performs RESTful webservice invocations
     * http://clinerpssh.ddns.net/RaziosWebSerivce/search/profissional?especializacao=abc
     *
     * @param params
     */
    public void invokeWS(RequestParams params) {
        // Show Progress Dialog
        prgDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://10.0.2.2:8084/RaziosWebService/search/anuncio", params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int i, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(new String(responseBody));

                    Log.d("onSuccess", obj.toString());
                    Log.d("onSuccess", String.valueOf(obj.getBoolean("status")));

                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
//                        Log.d(this.getClass().getSimpleName(), obj.toString());
                        processarInformacoesDeProfissionais(obj);
                        // Navigate to Home screen
//                            navigatetoHomeActivity();
                    }
                    // Else display error message
                    else {
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int i, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable throwable) {
                // Hide Progress Dialog
                //                prgDialog.hide();
                // When Http response code is '404'
                if (i == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (i == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void processarInformacoesDeProfissionais(JSONObject obj) {
        Intent intent = new Intent(this, ProfissionaisActivity.class);

        Iterator<String> keys = obj.keys();
        String key = keys.next();

        int index = 0;
        while (!key.equals("tag")) {

            Log.d(this.getClass().getSimpleName(), "profissional: " + key);

            JSONObject value = null;
            AnuncioInfo pedido = null;
            try {
                value = new JSONObject(obj.getString(key));
                pedido = new AnuncioInfo(value.getString("servicos"), value.getString("descricao"), value.getString("categoria"),
                        value.getString("subcategoria"), value.getString("email"), value.getString("nome"), value.getString("sobrenome"),
                        value.getString("data_inicio"), value.getString("telefone"));
                Log.d(this.getClass().getSimpleName(), pedido.getEmail());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            intent.putExtra(String.valueOf(index), pedido);

            index++;
            key = keys.next();
//            dao.addSPedido(pedido);
        }

        prgDialog.hide();
        startActivity(intent);
    }
}