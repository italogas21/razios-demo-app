package com.example.android.demoapp;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.android.demoapp.database.AnuncioInfo;
import com.example.android.demoapp.database.Pedido;

public class PerfilActivity extends AppCompatActivity {

    private AnuncioInfo dadosProfissional;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        dadosProfissional = (AnuncioInfo) getIntent().getSerializableExtra("pedido");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Perfil");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView tv_title = (TextView) findViewById(R.id.cardview_list_title);
        String nome = dadosProfissional.getNome() + " " + dadosProfissional.getSobrenome();
        tv_title.setText(nome);

        TextView tv1 = (TextView) findViewById(R.id.cardview_list_title1);

        TextView tv_servicos = (TextView) findViewById(R.id.tv_servicos);
        tv_servicos.setText(dadosProfissional.getServicos());

        TextView tv_descricao = (TextView) findViewById(R.id.tv_description);
        tv_descricao.setText(dadosProfissional.getDescricao());

        String categoria = getApplicationContext().getResources().getStringArray(R.array.categorias)[Integer.parseInt(dadosProfissional.getCategoria())-1];
        String sub_categoria = "";
        switch (categoria) {
            case "Assist.Tecnica":
                sub_categoria = getApplicationContext().getResources().getStringArray(R.array.assist_tecnica)[Integer.parseInt(dadosProfissional.getSubcategoria())-1];
                break;
            case "Reformas":
                sub_categoria = getApplicationContext().getResources().getStringArray(R.array.reformas)[Integer.parseInt(dadosProfissional.getSubcategoria())-1];
                break;
            default:
                Log.d(this.getClass().getSimpleName(), "Categoria nao encontrada");
        }

        tv1.setText(nome + " - " + sub_categoria);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
