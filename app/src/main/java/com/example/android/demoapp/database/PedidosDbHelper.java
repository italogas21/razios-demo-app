package com.example.android.demoapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by italo on 28/09/17.
 */
public class PedidosDbHelper extends SQLiteOpenHelper {

    // The name of the database
    private static final String DATABASE_NAME = "pedidosDb.db";
    // If you change the database schema, you must increment the database version
    private static final int VERSION = 4;

    // Constructor
    public PedidosDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    /**
     * Called when the tasks database is created for the first time.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create tasks table (careful to follow SQL formatting rules)
        final String CREATE_TABLE = "CREATE TABLE "  + PedidosContract.PedidosEntry.NOME_TABELA + " (" +
                PedidosContract.PedidosEntry._ID + " INTEGER PRIMARY KEY, " +
                PedidosContract.PedidosEntry.COLUNA_CATEGORIA + " TEXT NOT NULL, " +
                PedidosContract.PedidosEntry.COLUNA_SUBCATEGORIA + " TEXT NOT NULL, " +
                PedidosContract.PedidosEntry.COLUNA_DATA + " TEXT NOT NULL, " +
                PedidosContract.PedidosEntry.COLUNA_NUM_ANUNCIOS + " TEXT NOT NULL);";

        db.execSQL(CREATE_TABLE);
    }

    /**
     * This method discards the old table of data and calls onCreate to recreate a new one.
     * This only occurs when the version number for this database (DATABASE_VERSION) is incremented.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PedidosContract.PedidosEntry.NOME_TABELA);
        onCreate(db);
    }
}

