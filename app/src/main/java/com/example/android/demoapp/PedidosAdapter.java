package com.example.android.demoapp;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.demoapp.database.Pedido;
import com.example.android.demoapp.database.PedidoDAOImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by italo on 21/08/17.
 */

public class PedidosAdapter extends RecyclerView.Adapter<PedidosAdapter.ListItemViewHolder> {

    private static final String TAG = PedidosAdapter.class.getSimpleName();
    private static int viewHolderCount;

    private Context ctx;
    private Cursor mCursor;

    private int numberOfItems;
    private List<Pedido> listaDePedidos;
    private Map<String, List<Pedido>> listaDePedidosPorCategoria;
    private List<String> listaDeChaves;

    /*
     * An on-click handler that we've defined to make it easy for an Activity to interface with
     * our RecyclerView
     */
    final private PedidosAdapterOnClickHandler mClickHandler;

    /**
     * The interface that receives onClick messages.
     */
    public interface PedidosAdapterOnClickHandler {
        void onClick(Pedido pedido);
    }

    public PedidosAdapter(Context context, int numListItems, PedidosAdapterOnClickHandler clickHandler) {

        viewHolderCount = 0;
        this.ctx = context;
        this.numberOfItems = numListItems;
        this.mClickHandler = clickHandler;

        this.listaDePedidos = PedidoDAOImpl.getInstance().getAllPedidos();

//        processarListaDePedidos();

        Log.d(this.getClass().getSimpleName(), "em PedidosAdapter, lista: " + listaDePedidos.toString());

    }

//    /**
//     * Funcao classifica pedidos na lista de acordo com categoria.
//     */
//    private void processarListaDePedidos() {
//        listaDePedidosPorCategoria = new HashMap<String, List<Pedido>>();
//
//        for ( Pedido p : this.listaDePedidos ) {
//            String categoria = String.format("%s.%s", p.getCategoria(), p.getSubcategoria());
//            if (!listaDePedidosPorCategoria.containsKey(categoria)) {
//                ArrayList<Pedido> pedidosCategoria = new ArrayList<>();
//                pedidosCategoria.add(p);
//                listaDePedidosPorCategoria.put(categoria, pedidosCategoria);
//            } else {
//                listaDePedidosPorCategoria.get(categoria).add(p);
//            }
//        }
//
//        Iterator<String> keySet = listaDePedidosPorCategoria.keySet().iterator();
//        listaDeChaves = new ArrayList<String>();
//        while (keySet.hasNext()) {
//            listaDeChaves.add(keySet.next());
//        }
//    }

    /**
     * Called when RecyclerView needs a new {@link ListItemViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ListItemViewHolder, int)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ListItemViewHolder, int)
     */
    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.list_pedidos_2;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        ListItemViewHolder viewHolder = new ListItemViewHolder(view);

//        viewHolder.viewHolderIndex.setText("ViewHolder index: " + viewHolderCount);

//        int backgroundColorForViewHolder = ColorUtils.getViewHolderBackgroundColorFromInstance(context, viewHolderCount);
//        viewHolder.itemView.setBackgroundColor(backgroundColorForViewHolder);

        viewHolderCount++;
        Log.d(TAG, "onCreateViewHolder: number of ViewHolders created: " + viewHolderCount);
        return viewHolder;
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ListItemViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ListItemViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(ListItemViewHolder, int)} instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
            */
    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        Log.d(TAG, "#" + position);

        Pedido pedido = listaDePedidos.get(position);
//        List<Pedido> pedidos = listaDePedidosPorCategoria.get(listaDeChaves.get(position));

        String[] categorias = this.ctx.getResources().getStringArray(R.array.categorias);
        String categoria = categorias[Integer.parseInt(pedido.getCategoria())-1];

        String sub_categoria = "";
        switch (categoria) {
            case "Assist.Tecnica":
                sub_categoria = this.ctx.getResources().getStringArray(R.array.assist_tecnica)[Integer.parseInt(pedido.getSubcategoria())-1];
                break;
            case "Reformas":
                sub_categoria = this.ctx.getResources().getStringArray(R.array.reformas)[Integer.parseInt(pedido.getSubcategoria())-1];
                break;
            default:
                Log.d(this.getClass().getSimpleName(), "Categoria nao encontrada");
        }

        holder.categoriaTextView.setText(categoria);
        holder.especializacaoTextView.setText(sub_categoria);
//
//        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        Date date = new Date();

        String data = "Pedido feito em: " + pedido.getData();
        holder.dataTextView.setText(data);
        String selecao =  pedido.getNumeroAnuncios() + " Profissionais selecionados. ";
        holder.selecaoTextView.setText(selecao);
//        holder.imageTView.setImageURI(viewHolderData.);
//        holder.bind(position);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
//        return 0;
        return listaDePedidos.size();
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView selecaoTextView;
        private TextView dataTextView;
        private TextView categoriaTextView;
        private TextView especializacaoTextView;

        public ListItemViewHolder(View itemView) {
            super(itemView);

            categoriaTextView = (TextView) itemView.findViewById(R.id.tv_item_text);
            especializacaoTextView = (TextView) itemView.findViewById(R.id.firstLine);
            dataTextView = (TextView) itemView.findViewById(R.id.tv_item_data);
            selecaoTextView = (TextView) itemView.findViewById(R.id.tv_selecionados);

            itemView.setOnClickListener(this);

        }

        /**
         * This gets called by the child views during a click.
         *
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            Pedido pedido = listaDePedidos.get(adapterPosition);
//            List<Pedido> pedidos = listaDePedidosPorCategoria.get(listaDeChaves.get(adapterPosition));

//            String emails = "";
//            Iterator<Pedido> iterator = pedidos.iterator();
//            do {
//                Pedido next = iterator.next();
//
//                emails = emails + next.getEmail() + "|";
//
//                Log.d(PedidosAdapter.class.getSimpleName(), "onClick, chave: " + next.getEmail());
//            } while ( iterator.hasNext() );
//
//            Log.d(PedidosAdapter.class.getSimpleName(), "emails: " + emails);
//            params.put("emails",  emails);

//            invokeWS(params);

            mClickHandler.onClick(pedido);
        }

    }

//    private void processarInformacoesDeProfissionais(JSONObject obj) {
//
//        Iterator<String> keys = obj.keys();
//        String key = keys.next();
//
//        int index = 0;
//        while (!key.equals("tag")) {
//
//            Log.d(this.getClass().getSimpleName(), "profissional: " + key);
//
//            JSONObject value = null;
//            try {
//                value = new JSONObject(obj.getString(key));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            //ID do pedido eh configurado como uma string vazia
////            AnuncioInfo pedido = new AnuncioInfo(value.getString("email"), value.getString("nome"), value.getString("sobrenome"),
////                    value.getString("datainicio"), value.getString("telefone"));
//
//            key = keys.next();
////            dao.addSPedido(pedido);
//
//        }
}
