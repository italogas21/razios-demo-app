package com.example.android.demoapp.database;

import android.content.Context;

import com.example.android.demoapp.DemoApp;

/**
 * Created by italo on 02/10/17.
 */
public class ContextHelper
{
    private static final ContextHelper instance;
    private Context latestContext;

    static
    {
        instance = new ContextHelper();
    }
    private ContextHelper()
    {}

    public static ContextHelper getInstance()
    {
        return instance;
    }

    public Context getContext()
    {
        if (latestContext != null)
            return latestContext;

        return DemoApp.getAppContext();
    }
    public void setContext(Context ctxt)
    {
        latestContext = ctxt;
    }
}
