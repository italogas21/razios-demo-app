package com.example.android.demoapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.demoapp.utilities.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

public class CadastrarProfissionalActivity extends AppCompatActivity {

    private AlertDialog.Builder categoriaDialogBuilder;
    private AlertDialog.Builder subCategoriaDialogBuilder;

    private Button categoriasBtn;
    private Button subCategoriasBtn;

    private String[] categorias;
    private String[] subCategorias;

    private EditText edServicos;
    private EditText edDescricao;

    private RequestParams params;

    private ProgressDialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((SaveSharedPreference.getUserName(getApplicationContext()).length() == 0)) {
            Intent intent = new Intent(this, LoginActivity2.class);
            startActivity(intent);
        }

        setContentView(R.layout.activity_cadastrar_profissional);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Inserir Anúncio ");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Aguarde...");
        prgDialog.setCancelable(false);

        params = new RequestParams();

        edServicos = (EditText) findViewById(R.id.ed_servicos);
        edDescricao = (EditText) findViewById(R.id.ed_descricao);

        categorias = getResources().getStringArray(R.array.categorias);
        categoriasBtn = (Button) findViewById(R.id.categorias_button);

        subCategoriasBtn = (Button) findViewById(R.id.sub_categorias_button);

        categoriaDialogBuilder = new AlertDialog.Builder(this);
        categoriaDialogBuilder.setTitle("Escolha uma categoria: ")
        .setItems(categorias, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // The 'which' argument contains the index position
                // of the selected item
                String categoria = categorias[which];
                categoriasBtn.setText(categoria);

                setSubCategorias(which);

                params.put("categoria", String.valueOf(which + 1) );

                subCategoriaDialogBuilder.setItems(subCategorias, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        String subCategoria = subCategorias[which];
                        subCategoriasBtn.setText(subCategoria);

                        params.put("subcategoria", String.valueOf(which + 1) );

                        dialog.dismiss();
                    }
                });

                dialog.dismiss();
            }
        });

        subCategoriaDialogBuilder = new AlertDialog.Builder(this);
        subCategoriaDialogBuilder.setTitle("Escolha uma sub categoria: ");
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        getMenuInflater().inflate( R.menu.cadastrar_profissional, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_clean) {
            edDescricao.setText("");
            edServicos.setText("");
            categoriasBtn.setText("Selecionar Categoria");
            subCategoriasBtn.setText("Selecionar Sub Categoria");
        }

        return super.onOptionsItemSelected(item);
    }

    public void setSubCategorias(int which) {
        switch (which) {
            case 0:
                subCategorias = getResources().getStringArray(R.array.assist_tecnica);
                break;
            case 1:
                subCategorias = getResources().getStringArray(R.array.reformas);
                break;
            case 2:
                subCategorias = getResources().getStringArray(R.array.eventos);
                break;
            case 3:
                subCategorias = getResources().getStringArray(R.array.servicos_domesticos);
                break;
            case 4:
                subCategorias = getResources().getStringArray(R.array.aulas);
                break;
            case 5:
                subCategorias = getResources().getStringArray(R.array.moda_e_beleza);
                break;
            case 6:
                subCategorias = getResources().getStringArray(R.array.design_e_tecnologia);
                break;
            case 7:
                subCategorias = getResources().getStringArray(R.array.consultoria);
                break;
            case 8:
                subCategorias = getResources().getStringArray(R.array.saude);
                break;
            case 9:
                subCategorias = getResources().getStringArray(R.array.autos);
                break;
        }
    }

    public void selecionarCategoria(View view) {
        categoriaDialogBuilder.show();
    }

    public void selecionarSubCategoria(View view) {
        subCategoriaDialogBuilder.show();
    }

    public void cadastrarAnuncio( View view ){
        String servicos = String.valueOf(edServicos.getText());
        String descricao = String.valueOf(edDescricao.getText());
        // When Name Edit View, Email Edit View and Password Edit View have values other than Null
        if( Utility.isNotNull(servicos) && Utility.isNotNull(descricao) ) {
            // Put Http parameter name with value of Name Edit View control
            params.put("email", SaveSharedPreference.getUserEmail(getApplicationContext()));
            params.put("servicos", servicos);
            params.put("descricao", descricao);
            // Invoke RESTful Web Service with Http parameters
            invokeWS(params);
        } else {
            if( !Utility.isNotNull(servicos) ) {
                edServicos.setHint("Por favor, preencher servicos. ");
                edServicos.setHintTextColor(getResources().getColor(R.color.colorHint));
            }
            if( !Utility.isNotNull(descricao) ) {
                edDescricao.setHint("Por favor, preencher descricao. ");
                edDescricao.setHintTextColor(getResources().getColor(R.color.colorHint));
            }
        }
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void invokeWS(RequestParams params) {
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://10.0.2.2:8084/RaziosWebService/insert/doinsert", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    Log.d("onSuccess", new String(responseBody));
                    JSONObject obj = new JSONObject(new String(responseBody));

                // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
//                        setDefaultValues();
                        // Display successfully registered message using Toast
                        Toast.makeText(getApplicationContext(), "Seu anuncio foi inserido com sucesso!", Toast.LENGTH_LONG).show();
                        navigateToMainActivity();
                    }
                    // Else display error message
                    else {
//                        errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] bytes, Throwable throwable) {
                // Hide Progress Dialog
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void navigateToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
