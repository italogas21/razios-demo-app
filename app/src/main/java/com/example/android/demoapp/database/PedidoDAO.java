package com.example.android.demoapp.database;

import java.util.List;

/**
 * Created by italo on 30/09/17.
 */
public interface PedidoDAO {
    public List<Pedido> getAllPedidos();
    public Pedido getPedido(int rollNo);
    public void addSPedido(Pedido pedido) throws Exception;
    public void updateSPedido(Pedido pedido);
    public void deletePedido(Pedido pedido);
    public void deleteAllPedido();
}
