package com.example.android.demoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.android.demoapp.database.AnuncioInfo;
import com.example.android.demoapp.database.Pedido;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProfissionaisActivity extends AppCompatActivity implements ProfissionaisAdapter.ProfissionaisAdapterOnClickHandler {

    private static final int NUM_LIST_ITEMS = 10;
    private RecyclerView mRecyclerView;
    private ProfissionaisAdapter mAdapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profissionais);

        List<AnuncioInfo> pedidos = new ArrayList<AnuncioInfo>();
        int extras = getIntent().getExtras().size();
        Log.d(this.getClass().getSimpleName(), "list size: " + extras);
        Intent intent = getIntent();
        for (int i = 0; i < extras; i++) {
            pedidos.add((AnuncioInfo) intent.getSerializableExtra(String.valueOf(i)));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Profissionais selecionados");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_profissional_info);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setHasFixedSize(true);

        /*
         * CategoriasAdapter responsavel por mostrar cada item na lista.
         */
        mAdapter = new ProfissionaisAdapter(NUM_LIST_ITEMS, this, getApplicationContext());
        mAdapter.setProfissionais(pedidos);
        mRecyclerView.setAdapter(mAdapter);

        progressBar = (ProgressBar) findViewById(R.id.pb);
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(AnuncioInfo item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        intent.putExtra("pedido", (Serializable) item);
        startActivity(intent);
    }
}
