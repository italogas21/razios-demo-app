package com.example.android.demoapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by italo on 02/10/17.
 */
public class DemoApp extends Application {

    private static Context initialContext;

    @Override
    public void onCreate()
    {
        super.onCreate();
        initialContext = getApplicationContext();
    }

    public static Context getAppContext()
    {
        return initialContext;
    }
}
