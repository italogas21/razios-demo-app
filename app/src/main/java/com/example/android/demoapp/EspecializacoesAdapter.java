package com.example.android.demoapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by italo on 17/08/17.
 */

public class EspecializacoesAdapter extends RecyclerView.Adapter<EspecializacoesAdapter.ListItemViewHolder> {

    private static final String TAG = CategoriasAdapter.class.getSimpleName();
    private static int viewHolderCount;
    private int numberOfItems;

    final private EspecializacoesAdapterOnClickHandler mClickHandler;
    private String[] especializacoesArray;

    public interface EspecializacoesAdapterOnClickHandler {
        void onClick(int item);
    }

    public EspecializacoesAdapter(int numListItems, EspecializacoesAdapterOnClickHandler clickHandler, Context context) {
        this.numberOfItems = numListItems;
        this.mClickHandler = clickHandler;
        viewHolderCount = 0;

        especializacoesArray = context.getResources().getStringArray(R.array.assist_tecnica);
//        descricoesArray = context.getResources().getStringArray(R.array.descricoes);

    }

    /**
     * Called when RecyclerView needs a new {@link ListItemViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ListItemViewHolder, int)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ListItemViewHolder, int)
     */
    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.list_especializacoes;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        EspecializacoesAdapter.ListItemViewHolder viewHolder = new EspecializacoesAdapter.ListItemViewHolder(view);

        viewHolderCount++;
        Log.d(TAG, "onCreateViewHolder: number of ViewHolders created: " + viewHolderCount);
        return viewHolder;
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ListItemViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ListItemViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(ListItemViewHolder, int)} instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
        holder.titleTextView.setText(especializacoesArray[position]);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return especializacoesArray.length;
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView titleTextView;

        public ListItemViewHolder(View itemView) {
            super(itemView);

            titleTextView = (TextView) itemView.findViewById(R.id.tv_item_text);

            itemView.setOnClickListener(this);

        }

        /**
         * This gets called by the child views during a click.
         *
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            Log.d(CategoriasAdapter.class.getSimpleName(), "adapter position: " + String.valueOf(adapterPosition));
//            String itemClicked = especializacoesArray[adapterPosition];
            mClickHandler.onClick( (adapterPosition + 1) );
        }

    }


}
