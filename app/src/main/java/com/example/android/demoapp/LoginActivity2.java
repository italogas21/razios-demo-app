package com.example.android.demoapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.demoapp.utilities.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity2 extends AppCompatActivity {

    private TextInputEditText tvPassword;
    private TextInputEditText tvEmail;
    private TextView tvErrorMessageEmail;
    private TextView tvErrorMessagePassword;
    // Progress Dialog Object
    private ProgressDialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.login));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvEmail = (TextInputEditText) findViewById(R.id.email_2);
        tvPassword = (TextInputEditText) findViewById(R.id.password_2);

        tvErrorMessageEmail = (TextView) findViewById(R.id.tv_error_message_email);
        tvErrorMessagePassword = (TextView) findViewById(R.id.tv_error_message_password);

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Sending data ... ");

        Log.d("Login activity", "passei por onCreate");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loginUser(View view) {
        Log.d("Login activity", "passei por openSignUp");
        // Get Email Edit View Value
        String email = tvEmail.getText().toString();
        // Get Password Edit View Value
        String password = tvPassword.getText().toString();
        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();
        // When Email Edit View and Password Edit View have values other than Null
        if(Utility.isNotNull(email) && Utility.isNotNull(password)){
            if(Utility.validate(email)){
                // Put Http parameter username with value of Email Edit View control
                params.put("username", email);
                // Put Http parameter password with value of Password Edit Value control
                params.put("password", password);
                // Invoke RESTful Web Service with Http parameters
                invokeWS(params);
            }
            // When Email is invalid
            else {
                tvErrorMessageEmail.setVisibility(View.VISIBLE);
//                tvErrorMessagePassword.setVisibility(View.VISIBLE);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Por favor, preencher formulario. ", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method that performs RESTful webservice invocations
     * http://clinerpssh.ddns.net/RaziosWebSerivce/login/dologin?username=abc&password=xyz
     *
     * @param params
     */
    public void invokeWS( RequestParams params ){
        // Show Progress Dialog
        prgDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://10.0.2.2:8084/RaziosWebService/login/dologin",params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int i, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    // JSON Object
                    Log.d("onSuccess", new String(responseBody));
                    JSONObject obj = new JSONObject(new String(responseBody));

//                    String response = responseBody == null ? null : new String(responseBody, "UTF-8");
                    // When the JSON response has status boolean value assigned with true
                    if(obj.getBoolean("status")){
                        Toast.makeText(getApplicationContext(), "You are successfully logged in!", Toast.LENGTH_LONG).show();
                        SaveSharedPreference.setUserEmail(getApplicationContext(), obj.getString("email"));
                        SaveSharedPreference.setUserName(getApplicationContext(), obj.getString("nome") + " " + obj.getString("sobrenome"));
                        // Navigate to Home screen
                        navigatetoHomeActivity();
                    }
                    // Else display error message
                    else{
//                        errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(int i, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable throwable) {
                // Hide Progress Dialog
                prgDialog.hide();
                // When Http response code is '404'
                if(i == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(i == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void navigatetoHomeActivity() {
        Intent intent = new Intent(LoginActivity2.this, MainActivity.class);
//        try {
//            intent.putExtra("email", obj.getString("email"));
//            intent.putExtra("uname", obj.getString("uname"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        startActivity(intent);
    }

    public void abrirCadastrarActivity(View view) {
        Intent intent = new Intent(LoginActivity2.this, CadastrarActivity.class);
        startActivity(intent);
    }
}
