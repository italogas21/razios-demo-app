package com.example.android.demoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MinhaContaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minha_conta);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Minha Conta");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText ed_nome = (EditText) findViewById(R.id.ed_nome);
        ed_nome.setText(SaveSharedPreference.getUserName(MinhaContaActivity.this));

        EditText ed_mail = (EditText) findViewById(R.id.ed_email);
        ed_mail.setText(SaveSharedPreference.getUserEmail(MinhaContaActivity.this));


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void logOut(View view) {
        SaveSharedPreference.clearUserName(MinhaContaActivity.this);
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
