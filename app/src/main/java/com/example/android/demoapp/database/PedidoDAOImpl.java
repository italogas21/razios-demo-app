package com.example.android.demoapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by italo on 30/09/17.
 */
public class PedidoDAOImpl implements PedidoDAO {

    static PedidoDAOImpl instance = new PedidoDAOImpl();
    private List<Pedido> listaDePedidos;

    PedidosDbHelper dbHelper;

    private PedidoDAOImpl() {
        dbHelper = new PedidosDbHelper(ContextHelper.getInstance().getContext());
    }

    public static PedidoDAOImpl getInstance(){
        return instance;
    }

    @Override
    public List<Pedido> getAllPedidos() {

        if (listaDePedidos == null) {

            listaDePedidos = new ArrayList<Pedido>();

            SQLiteDatabase database = dbHelper.getReadableDatabase();

            Cursor mCursor = database.rawQuery(
                    "SELECT * FROM " + PedidosContract.PedidosEntry.NOME_TABELA + "",
                    null);
            mCursor.moveToFirst();

            Log.d(this.getClass().getSimpleName(), "em getAllPedidos(), cursor count: " + mCursor.getCount());

            try {
                 do {

                    String id = mCursor.getString(mCursor.getColumnIndex(PedidosContract.PedidosEntry._ID));
                    String categoria = mCursor.getString(mCursor.getColumnIndex(PedidosContract.PedidosEntry.COLUNA_CATEGORIA));
                    String sub_categoria = mCursor.getString(mCursor.getColumnIndex(PedidosContract.PedidosEntry.COLUNA_SUBCATEGORIA));;
                    String numDeAnuncios = mCursor.getString(mCursor.getColumnIndex(PedidosContract.PedidosEntry.COLUNA_NUM_ANUNCIOS));
                    String data = mCursor.getString(mCursor.getColumnIndex(PedidosContract.PedidosEntry.COLUNA_DATA));

                    Pedido p = new Pedido(id, categoria, sub_categoria, data, Integer.parseInt(numDeAnuncios));
                    listaDePedidos.add(p);
                 } while (mCursor.moveToNext());

            } finally {
                mCursor.close();
            }

            dbHelper.close();
            return listaDePedidos;

        } else {
            return listaDePedidos;
        }
    }

    @Override
    public Pedido getPedido(int rollNo) {
        return null;
    }

    @Override
    public void addSPedido(Pedido pedido) throws Exception {

        Log.d(this.getClass().getSimpleName(), "Adicionando pedido: " + pedido.getID());

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PedidosContract.PedidosEntry.COLUNA_CATEGORIA, pedido.getCategoria());
        values.put(PedidosContract.PedidosEntry.COLUNA_SUBCATEGORIA, pedido.getSubcategoria());
        values.put(PedidosContract.PedidosEntry.COLUNA_DATA, pedido.getData());
        values.put(PedidosContract.PedidosEntry.COLUNA_NUM_ANUNCIOS, pedido.getNumeroAnuncios());

        /* Insert ContentValues into database and get first row ID back */
        long rowId = database.insert(
                PedidosContract.PedidosEntry.NOME_TABELA,
                null,
                values);

        if(rowId == -1) {
            throw new Exception("Unable to insert into the database");
        }

        database.close();

    }

    @Override
    public void updateSPedido(Pedido pedido) {

    }

    @Override
    public void deletePedido(Pedido pedido) {

    }

    @Override
    public void deleteAllPedido() {

        Log.d(this.getClass().getSimpleName(), "Resetando BD");

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        database.delete(PedidosContract.PedidosEntry.NOME_TABELA, null, null);

        database.close();
    }
}
