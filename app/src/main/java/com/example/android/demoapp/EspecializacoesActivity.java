package com.example.android.demoapp;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.example.android.demoapp.database.Pedido;
import com.example.android.demoapp.database.PedidoDAOImpl;
import com.example.android.demoapp.database.PedidosContract;
import com.example.android.demoapp.database.PedidosDbHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class EspecializacoesActivity extends AppCompatActivity implements EspecializacoesAdapter.EspecializacoesAdapterOnClickHandler {

    private static final int NUM_LIST_ITEMS = 20;
    private RecyclerView mRecyclerView;
    private EspecializacoesAdapter mAdapter;
    // Progress Dialog Object
    private ProgressDialog prgDialog;

    private String categoria;
    private String subCategoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_especializacoes);

        categoria = getIntent().getStringExtra("categoria");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.selecionar_especializacao));
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_especializacoes);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setHasFixedSize(true);

        /*
         * CategoriasAdapter responsavel por mostrar cada item na lista.
         */
        mAdapter = new EspecializacoesAdapter(NUM_LIST_ITEMS, this, getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Buscando profissionais / Atualizando banco de dados ... ");
    }

    @Override
    public void onClick(int item) {
        Toast.makeText(this, String.valueOf(item), Toast.LENGTH_SHORT).show();

        subCategoria = String.valueOf(item);

        RequestParams params = new RequestParams();
        // Put Http parameter especializacao with value of Especializacao Text View control
        params.put("categoria", categoria);
        params.put("subcategoria", subCategoria) ;
        // Invoke RESTful Web Service with Http parameters
        invokeWS(params);
    }

    /**
     * Method that performs RESTful webservice invocations
     * http://clinerpssh.ddns.net/RaziosWebSerivce/search/profissional?especializacao=abc
     *
     * @param params
     */
    public void invokeWS(RequestParams params) {
        // Show Progress Dialog
        prgDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://10.0.2.2:8084/RaziosWebService/search/anuncios", params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int i, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject( new String(responseBody) );

                    Log.d("onSuccess", obj.toString());
                    Log.d("onSuccess", String.valueOf(obj.getBoolean("status")));

                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
//                        Log.d(this.getClass().getSimpleName(), obj.toString());
                        inserirPedidosNoBD(Integer.parseInt(obj.getString("num_de_anuncios")));
                        // Navigate to Home screen
                        navigatetoHomeActivity();
                    }
                    // Else display error message
                    else {
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int i, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable throwable) {
                // Hide Progress Dialog
//                prgDialog.hide();
                // When Http response code is '404'
                if (i == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (i == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void navigatetoHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void inserirPedidosNoBD(int numDeAnuncios) throws Exception {

//        prgDialog.setMessage("Atualizando banco de dados ... ");
//        prgDialog.show();

        Log.d(this.getClass().getSimpleName(), "Registrando pedido no BD");

        /* Use PedidoDAOImpl to get access to a writable database */
        PedidoDAOImpl dao = PedidoDAOImpl.getInstance();

        dao.deleteAllPedido();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        //ID do pedido eh configurado como uma string vazia
        Pedido pedido = new Pedido("", categoria, subCategoria, dateFormat.format(date), numDeAnuncios);

        dao.addSPedido(pedido);

        //reset
//        dao.deleteAllPedido();

//        Iterator<String> keys = obj.keys();
//        String key = keys.next();
//
//        int index = 0;
//        while (!key.equals("tag")) {
//
//            Log.d(this.getClass().getSimpleName(), "inserindo: " + key);
//
//            JSONObject value = new JSONObject(obj.getString(key));
//            int num = Integer.parseInt(value.getString("num_de_anuncios"));
//
//            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//            Date date = new Date();
//
//            //ID do pedido eh configurado como uma string vazia
//            Pedido pedido = new Pedido("", value.getString("email"), value.getString("servicos"), value.getString("descricao"),
//                    value.getString("categoria"), value.getString("subcategoria"));
//
//            key = keys.next();
//            dao.addSPedido(pedido);
        //        Log.d(this.getClass().getSimpleName(), "finalizando insercao de pedidos no bd. ");
        prgDialog.hide();
//
    }
}