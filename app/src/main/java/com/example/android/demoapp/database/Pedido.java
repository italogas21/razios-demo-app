package com.example.android.demoapp.database;

import java.io.Serializable;

/**
 * Created by italo on 30/09/17.
 */
public class Pedido implements Serializable {

    private String ID;
    private String categoria;
    private String subcategoria;
    private String data;
    private int numeroAnuncios;



    public Pedido(String ID, String categoria, String subcategoria, String data, int numDeAnuncios){
        this.setID(ID);
        this.setData(data);
        this.setNumeroAnuncios(numDeAnuncios);
        this.setCategoria(categoria);
        this.setSubcategoria(subcategoria);
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

    public String getData() { return data; }

    public void setData(String data) { this.data = data; }

    public int getNumeroAnuncios() { return numeroAnuncios; }

    public void setNumeroAnuncios(int numeroAnuncios) { this.numeroAnuncios = numeroAnuncios; }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
